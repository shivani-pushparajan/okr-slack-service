package global.coda.okr.slack.service.model;

public class CertificateType {
	private int certificateTypeId;
	private int issuerId;
	private String certificateTypeName;
	private CertificateIssuer certificateIssuer;
	private int count;

	public CertificateIssuer getCertificateIssuer() {
		return certificateIssuer;
	}

	public void setCertificateIssuer(CertificateIssuer certificateIssuer) {
		this.certificateIssuer = certificateIssuer;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getCertificateTypeId() {
		return certificateTypeId;
	}

	public int getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(int issuerId) {
		this.issuerId = issuerId;
	}

	public String getCertificateTypeName() {
		return certificateTypeName;
	}

	public void setCertificateTypeName(String certificateTypeName) {
		this.certificateTypeName = certificateTypeName;
	}

	public void setCertificateTypeId(int certificateTypeId) {
		this.certificateTypeId = certificateTypeId;
	}

	@Override
	public String toString() {
		return "CertificateType [certificateTypeId=" + certificateTypeId + ", issuerId=" + issuerId
				+ ", certificateTypeName=" + certificateTypeName + ", certificateIssuer=" + certificateIssuer
				+ ", count=" + count + "]";
	}

}
