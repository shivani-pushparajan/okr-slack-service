package global.coda.okr.slack.service.model;

import java.util.List;

public class CertificateIssuer {
	private int issuerId;
	private String issuerName;
	private int count;
	private List<CertificateType> certificateTypes;

	public int getCount() {
		return count;
	}

	public List<CertificateType> getCertificateTypes() {
		return certificateTypes;
	}

	public void setCertificateTypes(List<CertificateType> certificateTypes) {
		this.certificateTypes = certificateTypes;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(int issuerId) {
		this.issuerId = issuerId;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	@Override
	public String toString() {
		return "CertificateIssuer [issuerId=" + issuerId + ", issuerName=" + issuerName + ", count=" + count + "]";
	}

}
