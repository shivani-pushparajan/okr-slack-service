package global.coda.okr.slack.service.api;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import global.coda.okr.slack.service.model.CertificateIssuer;
import global.coda.okr.slack.service.model.GenericResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


public class SlackCommandLambdaService implements RequestHandler<Integer, List<CertificateIssuer>> {

    public List<CertificateIssuer> getOkrCertificateCommand(){
        String slackServiceUrl = "http://9d99a466.ngrok.io/api/slack_slash_command/okr_certificate";

        RestTemplate restTemplate=new RestTemplate();
        GenericResponse<Map<String, List<CertificateIssuer>>> commandResponse = restTemplate.postForObject(slackServiceUrl , null , GenericResponse.class);
        return commandResponse.getMessage().get("certificate_issuers");

    }

    @Override
    public List<CertificateIssuer> handleRequest(Integer input, Context context) {
        return getOkrCertificateCommand();
    }
}
