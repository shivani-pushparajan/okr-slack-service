package global.coda.okr.slack.service.model;

public class GenericResponse<T> {
	private boolean success;
	private T message;

	public GenericResponse(boolean success, T message) {
		this.success = success;
		this.message = message;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public T getMessage() {
		return message;
	}

	public void setMessage(T message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "GenericResponse [success=" + success + ", message=" + message
				+ "]";
	}

}
