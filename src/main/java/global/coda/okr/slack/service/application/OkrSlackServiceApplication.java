package global.coda.okr.slack.service.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"global.coda.okr.slack.service"})
public class OkrSlackServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(OkrSlackServiceApplication.class, args);
	}

}
